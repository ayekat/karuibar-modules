karuibar:memory
===============

This module displays the currently used main memory. It respects cache and
buffers, so one should get an accurate impression of the *really* used memory.

![memory.png](scrot/memory.png)

X resource                | Description
:-------------------------|:----------------------------------------------------
`karuibar.memory.meminfo` | File that is read to get memory usage information (default: `/proc/meminfo`) 

Module       | `memory`
:------------|:-------------------------------
Author       | Tinu Weber ([ayekat](https://gitlab.com/ayekat/))
Dependencies | None
Callbacks    | `init`, `poll`, `term`
License      | GPLv3
