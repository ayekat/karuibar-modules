#include <karuibar/karuibar.h>
#include <sys/sysinfo.h>
#include <string.h>
#include <errno.h>

/* structures */
struct data {
	char path[BUFSIZE];
	int icon;
};

/* functions */
int memory_init(struct module *m);
static int memory_poll(struct module *m);
static void memory_term(struct module *m);
static int read_val(FILE *f, char const *path, char const *property,
                    int unsigned *val);

/* variables */
static int unsigned long const icon_bitfield[15] = {
	11L, 12L, 0x000, 0x555, 0x000, 0x7FF, 0x7FF, 0x7FF, 0x7FF, 0x7FF, 0x7FF,
	          0x000, 0x555, 0x000
};

/* implementation */
int
memory_init(struct module *m)
{
	struct data *d;

	/* module data */
	m->data = d = smalloc(sizeof(struct data), "module data");

	/* user configuration */
	(void) xresources_string("meminfo", d->path, "/proc/meminfo");

	/* icons */
	d->icon = register_icon(icon_bitfield);

	/* module callbacks */
	m->poll = memory_poll;
	m->term = memory_term;

	/* initial poll */
	return memory_poll(m);
}

static int
memory_poll(struct module *m)
{
	FILE *f;
	int unsigned mem_total, mem_free, mem_buffers, mem_cached;
	int unsigned used, percentage;
	struct data *d = m->data;
	int retval = 0;

	/* alternative approach, that does however not respect cache memory:
	sysinfo info;
	sysinfo(&info);
	int stotal = info.totalram;
	int sused = stotal - info.freeram - info.bufferram;
	*/

	if ((f = fopen(d->path, "r")) == NULL) {
		ERROR("could not open %s for reading: %s",
		      d->path, strerror(errno));
		return -1;
	}

	if (read_val(f, d->path, "MemTotal", &mem_total) < 0
	|| read_val(f, d->path, "MemFree", &mem_free) < 0
	|| read_val(f, d->path, "MemAvailable", NULL) < 0
	|| read_val(f, d->path, "Buffers", &mem_buffers) < 0
	|| read_val(f, d->path, "Cached", &mem_cached) < 0) {
		retval = -1;
		goto memory_poll_out;
	}
	used = mem_total - mem_free - mem_buffers - mem_cached;
	percentage = used * 100 / MAX(mem_total, 1);

	/* update buffer */
	buf_clear();
	buf_append("\033F{%06X}%d%%\033r ", colour(100-percentage), percentage);
	buf_append("\033I{%u} \033m%.1fM\033r", d->icon, used/1024.0);

 memory_poll_out:
	(void) fclose(f);
	return retval;
}

static void
memory_term(struct module *m)
{
	sfree(m->data);
}

static int
read_val(FILE *f, char const *path, char const *property, int unsigned *val)
{
	int n;
	char format[BUFSIZE];

	(void) snprintf(format, BUFSIZE, "%s: %%%su kB\n",
	                property, val == NULL ? "*" : "");
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wformat-nonliteral"
	n = fscanf(f, format, val);
#pragma GCC diagnostic pop
	if (n < 0) {
		ERROR("could not read %s from %s: %s",
		      property, path, strerror(errno));
		return -1;
	} else if (n != 1 && val != NULL) {
		ERROR("could not match %s in %s", property, path);
		return -1;
	}
	return 0;
}
