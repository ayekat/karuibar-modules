#include <karuibar/karuibar.h>
#include <string.h>
#include <errno.h>

/* structures */
struct core {
	unsigned int busy_last, idle_last;
	unsigned int load;
};

struct data {
	char path_temp[BUFSIZE], path_stat[BUFSIZE];
	struct core *cores;
	unsigned int ncore;
	int temp_high, temp_hot, temp_crit;
	int icon;
	int hl_threshold_min;
};

/* functions */
int cpu_init(struct module *m);
static int cpu_poll(struct module *m);
static void cpu_term(struct module *m);

/* variables */
static unsigned int long const icon_bitfield[15] = {
	11L, 13L, 0x000, 0x555, 0x154, 0x603, 0x0F8, 0x6FB, 0x0F8, 0x6FB, 0x0F8,
	          0x603, 0x154, 0x555, 0x000
};
static char const *paths_temp[] = {
	"/sys/class/hwmon/hwmon0/device/temp1_input",
	"/sys/class/hwmon/hwmon0/temp1_input",
	"/sys/class/hwmon/hwmon1/device/temp1_input",
	"/sys/class/hwmon/hwmon1/temp1_input",
};

/* implementation */
int
cpu_init(struct module *m)
{
	unsigned int i;
	FILE *f;
	char buf[BUFSIZE];
	struct data *d;

	/* module data */
	m->data = d = smalloc(sizeof(struct data), "module data");

	/* user configuration */
	(void) xresources_string("temperature", d->path_temp, "");
	(void) xresources_string("stat", d->path_stat, "/proc/stat");
	(void) xresources_integer("high", &d->temp_high, 50);
	(void) xresources_integer("hot", &d->temp_hot, 65);
	(void) xresources_integer("crit", &d->temp_crit, 95);
	(void) xresources_integer("hl_threshold_min", &d->hl_threshold_min, 5);

	/* temperature path */
	if (d->path_temp[0] == '\0') {
		for (i = 0; i < sizeof(paths_temp); ++i) {
			f = fopen(paths_temp[i], "r");
			if (f != NULL) {
				(void) fclose(f);
				strcpy(d->path_temp, paths_temp[i]);
				break;
			}
		}
	}
	if (d->path_temp == NULL) {
		ERROR("could not find temperature file");
		return -1;
	}
	VERBOSE("using %s as temperature path", d->path_temp);

	/* CPU stats path */
	f = fopen(d->path_stat, "r");
	if (f == NULL) {
		ERROR("could not find CPU statistics file %s", d->path_stat);
		return -1;
	}
	for (d->ncore = 0;; ++d->ncore) {
		if (fscanf(f, "%*[^\n]\n%s", buf) < 1) {
			ERROR("could not read values from %s", d->path_stat);
			(void) fclose(f);
			return -1;
		}
		if (strncmp(buf, "cpu", 3) != 0)
			break;
	}
	(void) fclose(f);

	/* cores */
	d->cores = scalloc(d->ncore, sizeof(struct core), "CPU core list");
	for (i = 0; i < d->ncore; ++i) {
		d->cores[i].busy_last = 0;
		d->cores[i].idle_last = 0;
		d->cores[i].load = 0;
	}

	/* icon */
	d->icon = register_icon(icon_bitfield);

	/* module callbacks */
	m->poll = cpu_poll;
	m->term = cpu_term;

	/* initial update */
	return cpu_poll(m);
}

static int
cpu_poll(struct module *m)
{
	FILE *f;
	unsigned int i;
	unsigned int busy_tot, idle_tot, busy_diff, idle_diff, usage;
	unsigned int user, nice, system, idle, iowait, irq, softirq, steal,
	             guest, guest_nice;
	struct data *d = m->data;
	int temp, ret, saturated = 0;
	bool has_th, had_th;

	m->bg = karuibar.xresources.background;

	/* usage */
	f = fopen(d->path_stat, "r");
	if (f == NULL) {
		ERROR("failed to open %s for reading: %s\n",
		      d->path_stat, strerror(errno));
		return -1;
	}
	if (fscanf(f, "%*[^\n]\n") < 0) {
		/* ignore first line */
		ERROR("could not read from %s: %s", d->path_stat,
		      strerror(errno));
		(void) fclose(f);
		return -1;
	}
	for (i = 0; i < d->ncore; i++) {
		ret = fscanf(f, "%*[^ ] %u %u %u %u %u %u %u %u %u %u%*[^\n]\n",
		             &user, &nice, &system, &idle, &iowait, &irq,
		             &softirq, &steal, &guest, &guest_nice);
		if (ret < 10) {
			(void) fclose(f);
			ERROR("could not read from %s: %s\n", d->path_stat,
			      ret < 0 ? strerror(errno) : "failed match");
			return -1;
		}

		/* calculate load */
		busy_tot = user+nice+system+irq+softirq+steal+guest+guest_nice;
		busy_diff = busy_tot - d->cores[i].busy_last;
		idle_tot = idle + iowait;
		idle_diff = idle_tot - d->cores[i].idle_last;
		usage = idle_diff + busy_diff;
		d->cores[i].load = (busy_diff*1000+5)/10;
		if (usage > 0)
			d->cores[i].load /= usage;
		if (d->cores[i].load >= 98)
			saturated = 1;
		d->cores[i].busy_last = busy_tot;
		d->cores[i].idle_last = idle_tot;
	}
	(void) fclose(f);

	if (saturated)
		m->bg = karuibar.xresources.faded;

	/* temperature */
	f = fopen(d->path_temp, "r");
	if (f == NULL) {
		ERROR("failed to open file: %s", d->path_temp);
		return -1;
	}
	ret = fscanf(f, "%d", &temp);
	if (ret < 1) {
		ERROR("failed to read from %s: %s", d->path_temp,
		      ret < 0 ? strerror(errno) : "failed match");
		(void) fclose(f);
		return -1;
	}
	(void) fclose(f);
	temp /= 1000;

	/* module buffer */
	buf_clear();
	buf_append("\033%c%d°C\033r",
	           temp >= d->temp_crit ? 'e' : temp >= d->temp_hot ? 'w' :
	           temp >= d->temp_high ? 'm' : 'r', temp);
	buf_append(" \033%c\033I{%d} ", saturated ? 'w' : 'r', d->icon);
	has_th = false;
	for (i = 0; i < d->ncore; ++i) {
		had_th = has_th;
		has_th = (int signed) d->cores[i].load > d->hl_threshold_min;
		buf_append("\033%c|\033%c%2d%%%s\033r",
		           had_th || has_th ? 'm' : 'f',
		           has_th ? 'm' : 'f',
		           d->cores[i].load, i == d->ncore - 1 ? "|" : "");
	}
	return 0;
}

static void
cpu_term(struct module *m)
{
	struct data *d = m->data;

	sfree(d->cores);
	sfree(d);
}
