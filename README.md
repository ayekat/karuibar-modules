karuibar modules
================

This repository is a collection of officially supported and developed modules
for [karuibar](https://gitlab.com/ayekat/karuibar).


build
-----

In order to build, karuibar must be installed on the system (as its headers are
required for building the modules).

Each module can be built separately. This leaves the freedom to choose not to
compile certain modules (not because of size/time, but dependencies, e.g. the
[mpd](mpd) module requiring `libmpdclient` to be installed on your system).

Inside a module's directory, this will compile the module:

	make

Alternatively (i.e. you know that all the modules will build fine), from the
project root, you can also run something like

	for m in */; do cd $m; make; cd -; done

If the compilation fails, it is very likely that the karuibar headers were
installed to a different path; you will need to adapt the `INCLUDEDIR` variable
when compiling, either by passing it directly on the command line like

	make INCLUDEDIR=$HOME/.local/include

or by adding a config.mk in the project root directory containing something like

	INCLUDEDIR = ${HOME}/.local/include


install
-------

Modules are installed in `/usr/local/share/karuibar/modules` by default.

	make install

As for the build process, the location can be changed through environment
variables:

	make install MODULEDIR=$HOME/.local/share/karuibar/modules

or, in config.mk:

	MODULEDIR = ${HOME}/.local/share/karuibar/modules


`INSTALLDIR`
------------

Instead of setting `INCLUDEDIR` and `MODULEDIR` separately, you may also just
set `INSTALLDIR`, in which case `INCLUDEDIR` = `INSTALLDIR/include/karuibar`
and `MODULEDIR` = `INSTALLDIR/share/karuibar/modules`.

So the above examples for build and install can both be replaced by

	make (install) INSTALLDIR=$HOME/.local

or, in config.mk:

	INSTALLDIR = ${HOME}/.local


writing your own module
-----------------------

See the [documentation](DOCUMENTATION.md) and get your feet wet!
