karuibar:network
================

This module shows all network interfaces that have an IP address assigned, with
an icon next to it depending on the type of network interface (wireless,
ethernet, VDE, docker, VPN).

![network.png](scrot/network.png)

X resource                  | Description
:---------------------------|:---------------------------------------------
`karuibar.network.wireless` | Location of file that contains wireless interface information (default: `/proc/net/wireless`) 

Module       | `network`
:------------|:-------------------------------
Author       | Tinu Weber ([ayekat](https://gitlab.com/ayekat/))
Dependencies | None
Callbacks    | `init`, `poll`, `react`, `term`
License      | GPLv3
