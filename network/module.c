/* use GNU extensions for certain ioctl operations/types */
#define _DEFAULT_SOURCE

#include <karuibar/karuibar.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <net/if.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <linux/rtnetlink.h>

/* macros */
#define NUMIFS 10 /* maximum number of interfaces */

/* enumerations */
enum nettype { NET_UNKNOWN, NET_DOCKER, NET_ETH, NET_QEMU, NET_TUN, NET_USB,
               NET_VDE, NET_WLAN };

/* structures */
struct data {
	struct sockaddr_nl addr;
	struct netif **netifs;
	size_t numifs;
	char path_wifi[BUFSIZE];
	int icon_docker, icon_eth, icon_qemu, icon_tun, icon_usb, icon_vde;
	int icons_wlan[5];
};

struct netif {
	char name[BUFSIZE], addr[BUFSIZE];
	enum nettype type;
	int up;
	unsigned int quality;
};

/* functions */
int network_init(struct module *m);
static int network_poll(struct module *m);
static int network_react(struct module *m);
static void network_term(struct module *m);
static void attach_netif(struct data *d, struct netif *iface);
static void clear_netifs(struct data *d);
static struct netif *init_netif(int sock, struct ifreq *req);
static int scan_netifs(struct data *d);
static void show(struct data *d);
static void wlan_poll(struct netif *iface, char const *path_wifi);

/* variables */
static unsigned int long const icon_bitfield_docker[15] = {
	12, 13, 0x000, 0x000, 0x060, 0x0A4, 0x3B3, 0x296, 0xFFA, 0x805, 0x904,
	        0xC08, 0x730, 0x3C0, 0x000,
};
static unsigned int long const icon_bitfield_eth[15] = {
	11, 13, 0x000, 0x000, 0x000, 0x1FC, 0x104, 0x707, 0x401, 0x401, 0x401,
	        0x555, 0x555, 0x7FF, 0x000,
};
static unsigned int long const icon_bitfield_qemu[15] = {
	12, 13, 0x000, 0x0F0, 0x31C, 0x606, 0x63E, 0xE7F, 0xE3F, 0xE1F, 0xF9F,
	        0x7DE, 0x7DE, 0x3FE, 0x0F7,
};
static unsigned int long const icon_bitfield_tun[15] = {
	11, 13, 0x000, 0x000, 0x070, 0x1FC, 0x3FE, 0x3FE, 0x7FF, 0x7DF, 0x78F,
	        0x78F, 0x78F, 0x78F, 0x000,
};
static unsigned int long const icon_bitfield_usb[15] = {
	11, 13, 0x000, 0x3FE, 0x000, 0x3FE, 0x3FE, 0x3FE, 0x272, 0x272, 0x3FE,
	        0x3FE, 0x38E, 0x3FE, 0x3FE,
};
static unsigned int long const icon_bitfield_vde[15] = {
	12, 13, 0x550, 0x550, 0x553, 0x552, 0x000, 0xFFF, 0xAAD, 0xFFF, 0xAAF,
	        0xFFF, 0x000, 0x550, 0x550,
};
static unsigned int long const icon_bitfields_wlan[5][15] = {
	{ 11, 13, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000,
	          0x000, 0x000, 0x6DB, 0x000 },
	{ 11, 13, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x600,
	          0x600, 0x600, 0x6DB, 0x000 },
	{ 11, 13, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x000, 0x0C0, 0x0C0,
	          0x6C0, 0x6C0, 0x6DB, 0x000 },
	{ 11, 13, 0x000, 0x000, 0x000, 0x000, 0x018, 0x018, 0x0D8, 0x0D8, 0x6D8,
	          0x6D8, 0x6D8, 0x6DB, 0x000 },
	{ 11, 13, 0x000, 0x000, 0x003, 0x003, 0x01B, 0x01B, 0x0DB, 0x0DB, 0x6DB,
	          0x6DB, 0x6DB, 0x6DB, 0x000 },
};

int
network_init(struct module *m)
{
	unsigned int i;
	struct data *d;

	/* module data */
	m->data = d = smalloc(sizeof(struct data), "module data");
	d->numifs = 0;
	d->netifs = NULL;

	/* user configuration */
	xresources_string("wireless", d->path_wifi, "/proc/net/wireless");

	/* create socket */
	if ((m->fd = socket(AF_NETLINK, SOCK_RAW, NETLINK_ROUTE)) < 0) {
		ERROR("could not create link socket: %s", strerror(errno));
		return -1;
	}

	/* connect to interface subsystem */
	memset(&d->addr, 0, sizeof(d->addr));
	d->addr.nl_family = AF_NETLINK;
	d->addr.nl_groups = RTMGRP_LINK | RTMGRP_IPV4_IFADDR;
	if (bind(m->fd, (struct sockaddr *) &d->addr, sizeof(d->addr)) < 0) {
		ERROR("could not bind link socket to name: %s",strerror(errno));
		close(m->fd);
		return -1;
	}

	/* icons */
	d->icon_docker = register_icon(icon_bitfield_docker);
	d->icon_eth    = register_icon(icon_bitfield_eth);
	d->icon_qemu   = register_icon(icon_bitfield_qemu);
	d->icon_tun    = register_icon(icon_bitfield_tun);
	d->icon_usb    = register_icon(icon_bitfield_usb);
	d->icon_vde    = register_icon(icon_bitfield_vde);
	for (i = 0; i < 5; ++i)
		d->icons_wlan[i] = register_icon(icon_bitfields_wlan[i]);

	if (scan_netifs(d) < 0)
		return -1;

	/* module callbacks */
	m->poll = network_poll;
	m->react = network_react;
	m->term = network_term;

	/* initial poll */
	return network_poll(m);
}

static int
network_poll(struct module *m)
{
	struct data *d = m->data;
	unsigned int i;

	for (i = 0; i < d->numifs; ++i) {
		if (d->netifs[i]->type != NET_WLAN)
			continue;
		wlan_poll(d->netifs[i], d->path_wifi);
	}
	show(d);

	return 0;
}

static int
network_react(struct module *m)
{
	char buf[BUFSIZE];
	struct data *d = m->data;
	struct iovec iov = { .iov_base = buf, .iov_len = sizeof(buf) };
	struct msghdr msg = { .msg_name = &d->addr, .msg_namelen = sizeof(d->addr),
	                      .msg_iov = &iov, .msg_iovlen = 1 /* 1 iovec */ };

	recvmsg(m->fd, &msg, 0);
	if (scan_netifs(d) < 0)
		return -1;

	return network_poll(m);
}

static void
network_term(struct module *m)
{
	close(m->fd);
	sfree(m->data);
}

static void
attach_netif(struct data *d, struct netif *iface)
{
	d->netifs = srealloc(d->netifs, ++d->numifs*sizeof(struct netif *),
	                     "network interface list");
	d->netifs[d->numifs-1] = iface;
}

static void
clear_netifs(struct data *d)
{
	unsigned int i;

	for (i = 0; i < d->numifs; ++i)
		sfree(d->netifs[i]);
	if (d->netifs != NULL)
		sfree(d->netifs);
	d->numifs = 0;
	d->netifs = NULL;
}

static struct netif *
init_netif(int sock, struct ifreq *req)
{
	struct netif *iface;
	unsigned int ifaddr;

	/* not network interface */
	if (req->ifr_addr.sa_family != AF_INET)
		return NULL;

	/* cannot get interface address */
	if (ioctl(sock, SIOCGIFADDR, req) < 0)
		return NULL;

	/* is loopback interface */
	if (strcmp(req->ifr_name, "lo") == 0)
		return NULL;

	/* create interface structure */
	iface = smalloc(sizeof(struct netif), "network interface");
	snprintf(iface->name, BUFSIZE-1, "%s", req->ifr_name);
	ifaddr = ((struct sockaddr_in*) (&req->ifr_addr))->sin_addr.s_addr;
	snprintf(iface->addr, BUFSIZE-1, "%u.%u.%u.%u",
	          ifaddr        & 0xFF, (ifaddr >>  8) & 0xFF,
	         (ifaddr >> 16) & 0xFF, (ifaddr >> 24) & 0xFF);
	iface->up = ioctl(sock, SIOCGIFFLAGS, req) == 0 &&
	            (req->ifr_flags | IFF_UP) != 0;

	/* determine type */
	if (strncmp(iface->name, "docker", 6) == 0)
		iface->type = NET_DOCKER;
	else if (strncmp(iface->name, "eth", 3) == 0
	     || strncmp(iface->name, "en", 2) == 0)
		iface->type = NET_ETH;
	else if (strncmp(iface->name, "qemu", 4) == 0)
		iface->type = NET_QEMU;
	else if (strncmp(iface->name, "tun", 3) == 0
	     || strncmp(iface->name, "openvpn", 7) == 0)
		iface->type = NET_TUN;
	else if (strncmp(iface->name, "usb", 3) == 0)
		iface->type = NET_USB;
	else if (strncmp(iface->name, "vde", 3) == 0
	     || strncmp(iface->name, "ve-", 3) == 0)
		iface->type = NET_VDE;
	else if (strncmp(iface->name, "wlan", 4) == 0
	     || strncmp(iface->name, "wl", 2) == 0)
		iface->type = NET_WLAN;
	else
		iface->type = NET_UNKNOWN;

	return iface;
}

static int
scan_netifs(struct data *d)
{
	struct ifreq reqs[NUMIFS];
	struct ifconf conf;
	int sock;
	unsigned int i;
	size_t ifcount;
	struct netif *iface;

	/* create socket */
	if ((sock = socket(PF_INET, SOCK_DGRAM, 0)) <= 0) {
		ERROR("could not create query socket: %s", strerror(errno));
		return -1;
	}

	/* use ioctl to get interface information */
	conf.ifc_len = sizeof(reqs);
	conf.ifc_ifcu.ifcu_buf = (caddr_t) reqs;
	if (ioctl(sock, SIOCGIFCONF, &conf) < 0) {
		ERROR("could not perform query ioctl: %s", strerror(errno));
		close(sock);
		return -1;
	}

	/* create interface structures */
	clear_netifs(d);
	ifcount = (size_t) conf.ifc_len / sizeof(struct ifreq);
	for (i = 0; i < ifcount; ++i) {
		iface = init_netif(sock, &reqs[i]);
		if (iface == NULL)
			continue;
		attach_netif(d, iface);
	}

	close(sock);
	return 0;
}

static void
show(struct data *d)
{
	unsigned int i;
	struct netif *iface;

	buf_clear();

	if (d->numifs == 0)
		buf_append("no network");
	for (i = 0; i < d->numifs; ++i) {
		iface = d->netifs[i];

		/* separator */
		if (i > 0)
			buf_append("\033p\033s\033p");

		/* interface specific icons */
		switch (iface->type) {
		case NET_DOCKER:
			buf_append("\033I{%d} ", d->icon_docker);
			break;
		case NET_ETH:
			buf_append("\033I{%d} ", d->icon_eth);
			break;
		case NET_QEMU:
			buf_append("\033I{%d} ", d->icon_qemu);
			break;
		case NET_TUN:
			buf_append("\033I{%d} ", d->icon_tun);
			break;
		case NET_USB:
			buf_append("\033I{%d} ", d->icon_usb);
			break;
		case NET_VDE:
			buf_append("\033I{%d} ", d->icon_vde);
			break;
		case NET_WLAN:
			buf_append("\033F{%06X}\033I{%d}\033r ",
			           colour(iface->quality),
			           d->icons_wlan[MIN(iface->quality, 99)/20]);
			break;
		case NET_UNKNOWN:
			buf_append("%s:", iface->name);
		}

		buf_append("\033m%s\033r", iface->addr);
	}
}

static void
wlan_poll(struct netif *iface, char const *path_wifi)
{
	FILE *f;
	int q, ret;

	f = fopen(path_wifi, "r");
	if (f == NULL) {
		WARN("could not open %s for reading", path_wifi);
		iface->quality = 0;
		return;
	}
	ret = fscanf(f, "%*[^\n]\n%*[^\n]\n%*s %*d %d.%*s", &q);
	if (ret < 1) {
		WARN("wlan quality not available in %s:%s", path_wifi,
		     ret < 0 ? strerror(errno) : "failed match");
		iface->quality = 0;
	} else {
		iface->quality = (unsigned int) MAX(((double) q / 0.7), 0);
	}
	(void) fclose(f);
}
