karuibar:time
=================

This module display the date according to a user-defined date format.

![time.png](scrot/time.png)

X resource                    | Description
:-----------------------------|:---------------------------------------------
`karuibar.time.format`        | Date format (see the manual for `date (1)`; default: `%Y-%m-%d, %H:%M`)
`karuibar.time.use_localtime` | Use local timezone? (default: `yes`)

Module       | `time`
:------------|:-------------------------------------------------
Author       | Tinu Weber ([ayekat](https://gitlab.com/ayekat/))
Dependencies | None
Callbacks    | `init`, `poll`, `term`
License      | GPLv3
