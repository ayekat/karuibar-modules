#include <karuibar/karuibar.h>
#include <time.h>
#include <string.h>

/* structures */
struct data {
	char format[BUFSIZE];
	int use_localtime;
};

/* functions */
int time_init(struct module *m);
static int time_poll(struct module *m);
static void time_term(struct module *m);

/* implementation */
int
time_init(struct module *m)
{
	struct data *d;

	/* module data */
	m->data = d = smalloc(sizeof(struct data), "module data");

	/* user configuration */
	(void) xresources_string("format", d->format, "%Y-%m-%d, %H:%M");
	(void) xresources_boolean("use_localtime", &d->use_localtime, true);

	/* module functions */
	m->poll = time_poll;
	m->term = time_term;

	/* initial poll */
	return time_poll(m);
}

static int
time_poll(struct module *m)
{
	time_t rawtime;
	struct tm *date;
	struct data *d = m->data;

	rawtime = time(NULL);
	date = d->use_localtime ? localtime(&rawtime) : gmtime(&rawtime);
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wformat-nonliteral"
	strftime(m->buf, BUFSIZE, d->format, date);
#pragma GCC diagnostic push

	return 0;
}

static void
time_term(struct module *m)
{
	sfree(m->data);
}
