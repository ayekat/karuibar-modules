karuibar documentation
======================

This document consists of an introductory text of how to write a module for
karuibar, followed by a list of all functions, enumerations and structures
available in karuibar.h.


content
-------

* [writing a karuibar module](#writing-a-karuibar-module)
  * [overview](#overview)
  * [create a new module](#create-a-new-module)
    * [C file](#c-file)
    * [Makefile](#makefile)
    * [README](#readme)
    * [install & enable](#install--enable)
  * [callback functions](#callback-functions)
    * [initialising](#initialising)
    * [polling](#polling)
    * [reacting](#reacting)
    * [terminating](#terminating)
    * [the return value](#the-return-value)
  * [profiles](#profiles)
  * [tips & tricks](#tips--tricks)
* [karuibar.h](#karuibarh)
  * [functions](#functions)
  * [macros](#macros)
  * [the karuibar structure](#the-karuibar-structure)
  * [text formatting](#text-formatting)
* [contributing](#contributing)


writing a karuibar module
=========================

overview
--------

Your module will be a shared object that is dynamically loaded by karuibar at
runtime. It needs to implement a few (1-4) functions and handle the `module`
structure as defined in `karuibar/karuibar.h`.


create a new module
-------------------

A module is a pretty simple thing. It consists of a directory that typically
contains the following files:

* a C file containing your module's code
* a Makefile snippet that includes the global Makefile (usually one line)
* a README (optional, but recommended) describing your module

So in order to begin, we `mkdir` the directory named after our module, and start
hacking.


### C file

Create an arbitrarly named C file that will contain our module's code, and write
a minimal module that works:

``` c
#include <karuibar/karuibar.h>

int
mymodule_init(struct module *mod)
{
        (void) sprintf(mod->buf, "hello world!");
        return 0;
}
```

First thing you may want to know is: what is that `module` structure?

`struct module` is created for each instance of a module separately. It holds
all information about a module, such as the foreground and background colours,
the module name, or the text buffer that is displayed on the status bar. It will
get passed as an argument to each callback function, and *it is all yours*.

For now, we only use its `buf` field, which contains the text buffer that is
displayed on the bar. Here, it will display `hello world!` and return 0 to
indicate success.

That's it! A fully functional module.


### Makefile

Now we would like to compile this module. A simple Makefile containing

	include ../global.mk

is usually sufficient. In case you have additional library dependencies, you
should add them above, e.g.

	_LIBS = -lXss
	_CFLAGS = $(shell pkg-file --cflags x11)
	include ../global.mk

In order to generate the shared object `mymodule.so`, run

	make

Tadaaa! We've written and compiled our first module!


### README

Now that your module has been created, it is recommended to add a readme that
explains what your module does, who has written it, which callbacks it
implements (see the subsection [*callback functions*](#callback-functions)
below), under which licence you wish to publish it, which dependencies it has,
possibly a website link, a small screenshot/GIF to demonstrate what it looks
like, ...

Check out other modules readme files to get an idea about the recommended
structure.


### install & enable

Now, to make this new object available to karuibar, we need to install it to
whereever karuibar searches for modules (by default, it's one among
`$HOME/.local/share/karuibar/modules`, `/usr/local/share/karuibar/modules` and
`/usr/share/karuibar/modules`):

	make install

Next, we need to enable the module by modifying the `karuibar.modules` list in
the X resources file, and sourcing it with `xrdb`.

Once you (re)launch karuibar, it should show `hello world!` somewhere on it.


callback functions
------------------

Eventually you may want to display something more useful than of just a simple
string. There are two ways of updating information about whatever your module is
about: the first way is to *poll* for information on a regular basis; the second
way is to set up a file descriptor that can be listened to for events, and then
*react*.

karuibar associates the `module` structure to an instance of your module, and
contains 4 function pointers:

``` c
struct module {
        ...
        int (*init)(struct module *);
        int (*poll)(struct module *);
        int (*react)(struct module *);
        void (*term)(struct module *);
        ...
};
```

Note that, for karuibar, any function pointer that points to `NULL` is simply
assumed to be "not implemented", and thus not called. At start, all function
pointers (except `init`) are set to `NULL`.


### initialising

At startup, your initialisation function has already been assigned to the `init`
pointer by karuibar upon loading your module. It is thus **important** to name
it as seen before (otherwise karuibar will not find the symbol, and fail to load
your module).

The initialisation function is typically used to initialise data structures and
establish connections/states that are used throughout the entire lifetime of
your module.

It also assigns the function pointers for the other 3 callback functions, if
required.


### polling

The polling function is called periodically by karuibar every **n** seconds
(where **n** depends on the user configuration, assume 1 or 2 seconds). The idea
is to gather the necessary information, update the module's text buffer, and
return.

Polling is generally considered bad practice, but often you will find that the
part of the hardware/software your module communicates to does not provide any
interface that allows you to wait for events on their side. In these cases you
will have to refer to polling.

But don't worry, if you keep this function nicely small and juicy, it's not very
problematic. A setup may look as follows:

``` c
#include <karuibar/karuibar.h>

int mymodule_init(struct module *mod);
static int mymodule_poll(struct module *mod);

int
mymodule_init(struct module *mod)
{
        mod->poll = mymodule_poll;
        ...
}

static int
mymodule_poll(struct module *mod)
{
        /* poll for and update information */
        ...
}
```


### reacting

The *reacting* function is called whenever karuibar detects activity (typically
incoming data) on a provided file descriptor. But that file descriptor needs to
be defined by you during the initialisation:

```c
#include <karuibar/karuibar.h>

int mymodule_init(struct module *mod);
static int mymodule_react(struct module *mod);

int
mymodule_init(struct module *mod)
{
        mod->react = mymodule_react;
        mod->fd = ...  /* some procedure to obtain the file descriptor */
        ...
}

static int
mymodule_react(struct module *mod)
{
        /* read from file descriptor, update information */
        ...
}
```

Note that modules can only pass *one* file descriptor to karuibar. If you plan
to write a module that needs to listen on multiple file descriptors, you will
have to pass by an external process (this may likely be changed some day; stay
tuned!).


### terminating

Sometimes you allocate memory or open connections at the initialisation of your
module that need to be freed/closed at the end (typically when karuibar exits).

The termination function is where this happens:

```c
#include <karuibar/karuibar.h>

int mymodule_init(struct module *mod);
static int mymodule_term(struct module *mod);

int
mymodule_init(struct module *mod)
{
        mod->term = mymodule_term;
        ...
}

static void
mymodule_term(struct module *mod)
{
        /* free resources, close connections/files */
        ...
}
```


### the return value

You may have noticed that each callback function returns an integer value that
indicates either success (0) or failure (< 0).

In case you indicate failure at some point, karuibar will call your module's
`term` function (if present) and display an error message in the status bar.

This may not seem very nice, but it keeps things simple. If you want your module
to stay "alive" and handle the error on its own, return 0. But make sure you do
your job well and don't let the module code wreak havoc in handling errors.
After all, your callback functions are supposed to be small and fast.


profiles
--------

Users may invoke your module multiple times using different *profiles*. This
means that your `init`, `poll`, `react` and `term` functions may be called
multiple times by karuibar for more than one instance of a module.

Now, the modules are encapsulated in a structure instance, and that is fine. But
what happens with your own, static data? Is your data consistency guaranteed
also after multiple calls to your initialisation function?

In order to allow you to really instanciate *all* of your variables, structures
and whatelsenot, there is the `data` pointer in the module structure definition:

```c
struct module {
        ...
        void *data;
        ...
};
```

Since it is a `void *`, you can basically store anything to it (as long as it is
a pointer, of course). Typically, one would do something like

```c
#include <karuibar/karuibar.h>

...

struct data {
        /* your personal data */
};

...

int
mymodule_init(struct module *mod)
{
        struct data *d;
        ...
        mod->data = d = malloc(sizeof(struct data));
        d->...  /* initialise personal data */
        ...
}
```

Do not forget to properly free that structure in the `term` callback (actually,
using such a `data` structure will force you to have a `term` function defined
for your module).


tips & tricks
-------------

karuibar will draw before calling your first polling/reacting function, so it is
advised to have ready-to-use content in the module buffer at the end of your
module's initialisation. For modules that poll, it's pretty simple: at the end
of your initialisation function, put something like

```c
int
mymodule_init(struct module *mod)
{
        ...
        return mymodule_poll(mod);
}
```

For modules that do not have a polling function, it's recommended to split the
reacting function and the information gathering function, which will allow you
to do something similar to abovementioned:

```c
static int mymodule_react(struct module *);
static int update(struct module *);

int
mymodule_init(struct module *mod)
{
        ...
        mod->react = mymodule_react;
        return update(mod);
}

static int
mymodule_react(struct module *mod)
{
        ...  /* read from file descriptor */
        return update(mod);
}

static int
update(struct module *mod)
{
        ... /* gather information, update text buffer */
}
```


karuibar.h
==========

functions
---------

```c
void buf_append(char const *format, ...);
```

`sprintf`ing your stuff into the module buffer may be perfectly fine. But often
you won't add all the content to the buffer at once, but rather progressively.
In that case, `strcat`ing or otherwise inserting with calculating the offset
etc. is not very practical.

`buf_append` will append a formatted string to the already contained string in
the your module's buffer. Since karuibar knows which module it's currently
processing, `buf_append` will append to the right buffer.

---

```c
void buf_clear(void);
```

This function clears your module's buffer (actually it just sets the first
character to `\0`).

---

```c
uint32_t colour(unsigned int val);
```

Sometimes it may be nice to represent a value through a colour. This function
will gradually go from red (`val = 0`) to green (`val = 100`), and return that
colour as a 32 bit RGBA colour (even though alpha support is currently not
available in karuibar; `colour` will only set the RGB values).

---

```c
void print(FILE *out, enum log_level, char const *format, ...);
```

This will print a formatted string to the indicated output (stdout or stderr),
with an indicated log level, appended to a timestamp, for the logs. It is
however recommended **not to use this function directly**, but through the
following macros:

```c
DEBUG(...)
VERBOSE(...)
NOTE(...)
WARN(...)   /* => stderr */
ERROR(...)  /* => stderr */
```

They all take a format string.

---

```c
int register_icon(int long unsigned const *bitfield);
```

Icons in karuibar are passed around as bitfields, which are represented as an
array of `unsigned long` where the binary representation of each value
represents one row in the icon, with the most-significant bit to the left and
the least-significant bit to the right. The first two values are not part of the
icon, but specify the icon's width and height.

This limits an icon's width to `8*sizeof(long)` pixels, but that should be
enough for status bar icons.

`register_icon` takes such a bitfield and returns the icon ID, that can later be
used in the text buffer (see [#text formatting](#text-formatting) for how to).

---

```c
void *scalloc(size_t nmemb, size_t size, char const *context);
void *smalloc(size_t size, char const *context);
void *srealloc(void *ptr, size_t size, char const *context);
void sfree(void *);
```

These methods are just wrappers around the different memory allocation functions
provided by `stdlib.h`, and they behave similar, except that in case of a
failure, it makes dzenstat `exit()`.

Woah!

The argument behind this is that in case of a memory allocation failure we're
screwed anyway, so karuibar "sacrifices" itself by exiting, letting the OS clean
up the memory, and giving back some resources.

As a nice side effect, the pointer returned by these functions is guaranteed to
be valid, so you don't need to check it against `NULL`:

```c
        struct data *d = smalloc(sizeof(struct data), "module data");
        ...  /* go on, have a happy life */
```

And in case it fails, you won't get to live the consequences; the `context`
argument will be appended to the resulting error message:

	karuibar [2014-12-05|19:54] [mymodule] FATAL: could not allocate 24 bytes for module data
	user@host:~$ ▉

`sfree` was just added for completeness' sake. Furthermore this may allow you to
write your module without having to `#include <stdlib>`, despite all the memory
allocation/freeing.

---

```c
int xresources_boolean(char const *name, int *ret, int def);
int xresources_colour(char const *name, uint32_t *ret, uint32_t def);
int xresources_integer(char const *name, int *ret, int def);
int xresources_string(char const *name, char *ret, char const *def);
```

These functions will allow you to access X resource settings. `name` indicates
the resource name (without prefix; so `karuibar.mymodule.yadayada` may be
accessed through `name = "yadayada"`), `ret` indicates the resulting resource
value. In case the resource is not set by the user, `ret` takes the default
value as indicated by `def`.


macros
------

The macro `BUFSIZE` indicates a sane buffer size, currently at 512 bytes. It is
also the size of your module's text buffer.

The macros `LENGTH`, `MAX` and `MIN` may be helpful, too. They calculate the
number of elements in an array, and the maximum and minimum value among two
given parameters, respectively. Note that `LENGTH` should *never* be called on
dynamically allocated arrays.


the karuibar structure
----------------------

`struct { ... } karuibar` contains variables specific to karuibar itself. You
will probably only make use of the predefined markup colours (faded, emphasised,
warning, error), and karuibar's default foreground and background colour.

Note that content of this structure is not guaranteed to remain in future
versions, since some of karuibar's data is stored here and some in the base
source code as static variables, but currently there is not clear criteria for
what is declared where.


text formatting
---------------

karuibar has a basic formatting language that allows you to modify the
appearance of the text and insert icons. Every formatting element is an escape
sequence that changes the *state* of the drawing procedure (rather than a markup
element with begin/end tokens).

---

	\033F{XXXXXX}
	\033B{XXXXXX}

These sequences change the current foreground and background colour to what's
indicated by the hexadecimal RGB sequence `XXXXXX`, respectively.

---

	\033n

This sequence swaps the current foreground and background colour.

---

	\033f
	\033r
	\033m
	\033w
	\033e

These sequences set the foreground colour to predefined colours `Faded` (faded
out, weakly visible), `Reset` (karuibar's default foreground), `eMphasised`
(highlighted, well visible), `Warn` (to indicate a warning, usually yellow or
orange), and `Error` (to indicate an error, usually red).

---

	\033p
	\033s

These sequences skip forward to add a padding (`p`) and draw a separator (`s`).

---

	\033I{n}

This sequence draws a registered icon identified by the decimal ID `n`. See
[#register\_icon](#register_icon) for how to register icons and obtain an ID.


contributing
============

I cannot possibly think of all possible needs, so it is very likely that you
will implement a module that may be useful to lots of people. I will happily
accept new modules for karuibar. I don't ask for much more than to follow the
[Linux kernel coding style](https://www.kernel.org/doc/Documentation/CodingStyle),
with one important exception:

When implementing a function, the return type of a function is to be split
from the function name and to be put on a separate line above. This allows
grepping for function implementations more easily, looks nicer, and has the
additional benefit of saving columns (remember, there are only 80 of them).

```c
int
function(int x)
{
        /* function body */
}
```

That's all.
