karuibar:capslock
=================

This module indicates whether CapsLock is active. It's intended for machines
that do not ship with a physical LED indicator.

... you know, like those hip new, but unusable abominations they call
"Thinkpads" but don't deserve that name.

![capslock.png](scrot/capslock.png)

X resource                     | Description
:------------------------------|:--------------------------------------
`karuibar.capslock.foreground` | Foreground colour (default: `#222222`)
`karuibar.capslock.background` | Background colour (default: `#FF0000`)

Module       | `capslock`
:------------|:-------------------------------
Author       | Tinu Weber ([ayekat](https://gitlab.com/ayekat/))
Dependencies | X11
Callbacks    | `init`, `react`, `term`
License      | GPLv3
