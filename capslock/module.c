#include <karuibar/karuibar.h>
#include <X11/Xlib.h>
#include <X11/XKBlib.h>

/* structures */
struct data {
	Display *dpy;
	int xkb_base_event_type;
};

/* functions */
int capslock_init(struct module *m);
static int capslock_react(struct module *m);
static void capslock_term(struct module *m);
static int update(struct data *d);

/* implementation */
int
capslock_init(struct module *m)
{
	struct data *d;
	int err, reason;

	/* module data */
	m->data = d = smalloc(sizeof(struct data), "data");
	m->react = capslock_react;
	m->term = capslock_term;

	/* user configuration */
	(void) xresources_colour("foreground", &m->fg,
	                         karuibar.xresources.background);
	(void) xresources_colour("background", &m->bg,
	                         karuibar.xresources.error);

	/* X/XKB */
	d->dpy = XOpenDisplay(NULL);
	m->fd = ConnectionNumber(d->dpy);
	(void) XkbOpenDisplay(DisplayString(d->dpy), &d->xkb_base_event_type,
	                      &err, NULL, NULL, &reason);
	if (!XkbSelectEvents(d->dpy, XkbUseCoreKbd, XkbIndicatorStateNotifyMask,
	                     XkbIndicatorStateNotifyMask)) {
		ERROR("could not select XKB events");
		return -1;
	}
	return update(d);
}

static int
capslock_react(struct module *m)
{
	XEvent xev;
	struct data *d = m->data;

	if (!XCheckTypedEvent(d->dpy, d->xkb_base_event_type, &xev)) {
		WARN("unselected X event");
		return 0;
	}

	return update(d);
}

static void
capslock_term(struct module *m)
{
	struct data *d = m->data;

	(void) XkbSelectEvents(d->dpy, XkbUseCoreKbd,
	                       XkbIndicatorStateNotifyMask, 0x0);
	(void) XCloseDisplay(d->dpy);
	sfree(d);
}

static int
update(struct data *d)
{
	int unsigned state;
	int caps;

	/* get state */
	(void) XkbGetIndicatorState(d->dpy, XkbUseCoreKbd, &state);
	caps = (state & 0x01) == 1;

	/* display */
	buf_clear();
	if (caps)
		buf_append("CAPS");

	return 0;
}
