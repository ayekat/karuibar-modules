#include <karuibar/karuibar.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

/* functions */
int stdin_init(struct module *m);
static int stdin_react(struct module *m);
static int parse(char const *in);
static int parse_icon(char const *in);

/* variables */
static int setup, unique = 1;

/* implementation */
int
stdin_init(struct module *m)
{
	/* stdin is a "singleton" */
	if (!unique) {
		ERROR("this module cannot be instantiated multiple times");
		return -1;
	}
	unique = 0;

	m->react = stdin_react;
	m->fd = STDIN_FILENO;
	setup = 1;

	buf_clear();

	return 0;
}

static int
stdin_react(struct module *m)
{
	char const *tok;
	char in[BUFSIZE];
	ssize_t r;

	r = read(m->fd, in, BUFSIZE);
	if (r <= 0) {
		if (r < 0)
			ERROR("could not read: %s", strerror(errno));
		else
			ERROR("file descriptor %d closed", m->fd);
		return -1;
	}
	in[r] = '\0';

	tok = strtok(in, "\n");
	while (tok != NULL) {
		if ((setup ? parse_icon(tok) : parse(tok)) < 0)
			return -1;
		tok = strtok(NULL, "\n");
	}
	return 0;
}

static int
parse(char const *in)
{
	buf_clear();
	buf_append("%s", in);
	return 0;
}

static int
parse_icon(char const *in)
{
	int pos, n;
	size_t i;
	unsigned int w, h;
	unsigned int long *icon;

	if (in[0] == '.') {
		setup = 0;
		return 0;
	}

	if (sscanf(in, "%i,%i%n", (int *) &w, (int *) &h, &pos) < 2) {
		ERROR("icon dimension format mismatch");
		return -1;
	}
	icon = scalloc(h+2, sizeof(int long unsigned), "icon bitfield");
	icon[0] = w;
	icon[1] = h;
	for (i = 0; i < h; ++i) {
		if (sscanf(in+pos, ",%08lX%n", &icon[i+2], &n) < 1) {
			ERROR("icon data format mismatch");
			sfree(icon);
			return -1;
		}
		pos += n;
	}
	VERBOSE("registered icon ID %d", register_icon(icon));
	sfree(icon);

	return 0;
}
