karuibar:stdin
==============

This module displays formatted text read from stdin.

Note that this module cannot be instantiated multiple times.

X resource                  | Description
:---------------------------|:---------------------------------------------
`karuibar.stdin.foreground` | Foreground (text) colour
`karuibar.stdin.background` | Background colour

Module       | `time`
:------------|:-------------------------------------------------
Author       | Tinu Weber ([ayekat](https://gitlab.com/ayekat/))
Dependencies | None
Callbacks    | `init`, `react`
License      | GPLv3
