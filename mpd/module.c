#include <karuibar/karuibar.h>
#include <mpd/client.h>
#include <string.h>

/* structures */
struct data {
	struct mpd_connection *con;
	enum mpd_state state;
	int icon;
	char host[BUFSIZE];
	unsigned int port;
};

/* functions */
int mpd_init(struct module *m);
static int mpd_react(struct module *m);
static void mpd_term(struct module *m);
static int query(struct data *d);
static void sanitise(char *buf);
static void show(struct data *d, char const *artist, char const *album,
                 char const *title);
static int update(struct data *d);

/* variables */
static unsigned int long const icon_bitfield[15] = {
	8, 13, 0x00, 0x08, 0x0C, 0x0E, 0x0B, 0x09, 0x0B, 0x0A, 0x08, 0x78, 0xF8,
	       0x70, 0x00
};

/* implementation */
int
mpd_init(struct module *m)
{
	struct data *d;

	/* module data */
	m->data = d = smalloc(sizeof(struct data), "module data");

	/* user configuration */
	xresources_string("host", d->host, "localhost");
	xresources_integer("port", (int *) &d->port, 0);

	/* initialise connection to MPD */
	d->con = mpd_connection_new(d->host, d->port, 0);
	if (mpd_connection_get_error(d->con) != MPD_ERROR_SUCCESS) {
		ERROR("could not connect: %s",
		      mpd_connection_get_error_message(d->con));
		return -1;
	}
	if ((m->fd = mpd_connection_get_fd(d->con)) <= 0) {
		ERROR("could not get file descriptor: %s",
		      mpd_connection_get_error_message(d->con));
		return -1;
	}

	/* icon */
	d->icon = register_icon(icon_bitfield);

	/* module callbacks */
	m->term = mpd_term;
	m->react = mpd_react;

	/* initial poll */
	return query(d);
}

static int
mpd_react(struct module *m)
{
	struct data *d = m->data;

	mpd_recv_idle(d->con, true);
	if (mpd_connection_get_error(d->con) != MPD_ERROR_SUCCESS) {
		ERROR("could not receive wakeup: %s",
		      mpd_connection_get_error_message(d->con));
		return -1;
	}

	return query(d);
}

static void
mpd_term(struct module *m)
{
	struct data *d = m->data;

	mpd_connection_free(d->con);
	sfree(d);
}

static int
query(struct data *d)
{
	struct mpd_status *status;

	/* get state */
	mpd_send_status(d->con);
	if ((status = mpd_recv_status(d->con)) == NULL) {
		ERROR("could not get status: %s",
		      mpd_connection_get_error_message(d->con));
		return -1;
	}
	d->state = mpd_status_get_state(status);
	mpd_status_free(status);
	if (mpd_connection_get_error(d->con) != MPD_ERROR_SUCCESS) {
		ERROR("could not get state: %s",
		      mpd_connection_get_error_message(d->con));
		return -1;
	}

	/* check if we can get song info */
	buf_clear();
	if (d->state != MPD_STATE_STOP && d->state != MPD_STATE_UNKNOWN
	                               && d->state != MPD_STATE_PAUSE)
		if (update(d) < 0)
			return -1;

	/* request next event */
	if (!mpd_send_idle_mask(d->con, MPD_IDLE_PLAYER)) {
		ERROR("could not send idle mask: %s",
		      mpd_connection_get_error_message(d->con));
		return -1;
	}

	return 0;
}

static void
sanitise(char *buf)
{
	if (strlen(buf) > 23)
		strcpy(buf + 20, "...");
}

static void
show(struct data *d, char const *artist, char const *album, char const *title)
{
	(void) album;

	buf_append("\033F{%06X}\033I{%d}\033r %s » \033%c%s",
	           d->state == MPD_STATE_PLAY ? 0x00FF00
	                                      : karuibar.xresources.faded,
	           d->icon, artist,
	           d->state == MPD_STATE_PLAY ? 'm' : 'r', title);
}

static int
update(struct data *d)
{
	struct mpd_song *song;
	char title[BUFSIZE], artist[BUFSIZE], album[BUFSIZE];
	void const *p;

	/* get song info */
	mpd_send_current_song(d->con);
	while ((song = mpd_recv_song(d->con)) != NULL) {
		p = mpd_song_get_tag(song, MPD_TAG_TITLE, 0);
		strncpy(title, p == NULL ? "(nil)" : p, BUFSIZE - 1);
		sanitise(title);
		p = mpd_song_get_tag(song, MPD_TAG_ARTIST, 0);
		strncpy(artist, p == NULL ? "(nil)" : p, BUFSIZE - 1);
		p = mpd_song_get_tag(song, MPD_TAG_ALBUM, 0);
		strncpy(album, p == NULL ? "(nil)" : p, BUFSIZE - 1);
		artist[BUFSIZE - 1] = album[BUFSIZE - 1] = '\0';
		if (mpd_connection_get_error(d->con) != MPD_ERROR_SUCCESS) {
			ERROR("could not get song information: %s",
			      mpd_connection_get_error_message(d->con));
			return -1;
		}
	}
	show(d, artist, album, title);
	return 0;
}
