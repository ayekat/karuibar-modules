karuibar:alsa
=============

This is the ALSA module for [karuibar](https://gitlab.com/ayekat/karuibar). It
displays the volume level of a chosen card and channel, and whether it is muted
or not.

![alsa.gif](scrot/alsa.gif)

X resource              | Description
:-----------------------|:-------------------------------------------
`karuibar.alsa.card`    | Name of the sound card (default: `default`)
`karuibar.alsa.channel` | Channel (default: `Master`)

Module       | `alsa`
:------------|:-------------------------------------------------
Author       | Tinu Weber ([ayekat](https://gitlab.com/ayekat/))
Callbacks    | `init`, `react`, `term`
Dependencies | ALSA (`libasound.so`)
License      | GPLv3
