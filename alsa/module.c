#include <karuibar/karuibar.h>
#include <alsa/asoundlib.h>

/* structures */
struct data {
	int enabled, persistent, capture;
	snd_mixer_t *ctl;
	snd_mixer_elem_t *elem;
	long min, max, vol;
	char card[BUFSIZE], channel[BUFSIZE];
	int icons[4];
};

/* functions */
int alsa_init(struct module *m);
static int alsa_react(struct module *m);
static void alsa_term(struct module *m);
static int update(struct data *d);

/* variables */
static unsigned int long const icon_bitfields[4][15] = {
	{ 10L, 13L, 0x000, 0x000, 0x000, 0x040, 0x0C0, 0x3C0, 0x3C0, 0x3C0,
	            0x0C0, 0x040, 0x000, 0x000, 0x000 },
	{ 10L, 13L, 0x000, 0x000, 0x000, 0x040, 0x0C0, 0x3D0, 0x3D0, 0x3D0,
	            0x0C0, 0x040, 0x000, 0x000, 0x000 },
	{ 10L, 13L, 0x000, 0x000, 0x000, 0x048, 0x0C4, 0x3D4, 0x3D4, 0x3D4,
	            0x0C4, 0x048, 0x000, 0x000, 0x000 },
	{ 10L, 13L, 0x000, 0x004, 0x002, 0x049, 0x0C5, 0x3D5, 0x3D5, 0x3D5,
	            0x0C5, 0x049, 0x002, 0x004, 0x000 },
};
static unsigned int long const icon_mic_bitfield[15] = {
	9L, 13L, 0x000, 0x010, 0x082, 0x038, 0x145, 0x044,
	         0x038, 0x038, 0x038, 0x038, 0x038, 0x010, 0x000,
};

/* implementation */
int
alsa_init(struct module *m)
{
	unsigned int i;
	snd_mixer_selem_id_t *sid;
	struct pollfd pfd;
	struct data *d;

	/* module data */
	m->data = d = smalloc(sizeof(struct data), "data");

	/* user configuration */
	(void) xresources_string("card", d->card, "default");
	(void) xresources_string("channel", d->channel, "Master");
	(void) xresources_boolean("persistent", &d->persistent, 1);
	(void) xresources_boolean("capture", &d->capture, 0);

	/* initialise control interface */
	if (snd_mixer_open(&d->ctl, 0) < 0) {
		ERROR("failed to open control interface");
		goto alsa_init_error_ctl_clean;
	}
	if (snd_mixer_attach(d->ctl, d->card) < 0) {
		ERROR("failed to attach control interface");
		goto alsa_init_error_ctl_open;
	}
	if (snd_mixer_selem_register(d->ctl, NULL, NULL) < 0) {
		ERROR("failed to register control interface");
		goto alsa_init_error;
	}
	if (snd_mixer_load(d->ctl) < 0) {
		ERROR("failed to load control interface");
		goto alsa_init_error;
	}

	/* initialise mixer element */
	/* snd_mixer_selem_id_alloca(&sid); doesn't work with C99 */
	sid = smalloc(snd_mixer_selem_id_sizeof(), "mixer element");
	snd_mixer_selem_id_set_index(sid, 0);
	snd_mixer_selem_id_set_name(sid, d->channel);
	d->elem = snd_mixer_find_selem(d->ctl, sid);
	if (d->elem == NULL) {
		ERROR("could not get mixer element");
		goto alsa_init_error;
	}

	/* get poll file descriptor */
	if (snd_mixer_poll_descriptors(d->ctl, &pfd, 1) == 0) {
		ERROR("could not get file descriptor");
		goto alsa_init_error;
	}
	m->fd = pfd.fd;

	/* get volume range */
	if (d->capture)
		snd_mixer_selem_get_capture_volume_range(d->elem, &d->min, &d->max);
	else
		snd_mixer_selem_get_playback_volume_range(d->elem, &d->min, &d->max);

	/* icons */
	for (i = 0; i < LENGTH(icon_bitfields); ++i) {
		d->icons[i] = register_icon(d->capture ? icon_mic_bitfield
		                                       : icon_bitfields[i]);
	}

	/* module callbacks */
	m->react = alsa_react;
	m->term = alsa_term;

	/* initial poll */
	return alsa_react(m);

 alsa_init_error:
	snd_mixer_detach(d->ctl, d->card);
 alsa_init_error_ctl_open:
	snd_mixer_close(d->ctl);
 alsa_init_error_ctl_clean:
	sfree(d);
	return -1;
}

static int
alsa_react(struct module *m)
{
	struct data *d = m->data;
	snd_mixer_handle_events(d->ctl);
	return update(d);
}

static void
alsa_term(struct module *m)
{
	struct data *d = m->data;

	snd_mixer_detach(d->ctl, d->card);
	snd_mixer_close(d->ctl);
	sfree(d);
}

static int
update(struct data *d)
{
	long l, r;

	/* get level and state */
	if (d->capture) {
		snd_mixer_selem_get_capture_volume(d->elem, SND_MIXER_SCHN_MONO, &l);
		snd_mixer_selem_get_capture_volume(d->elem, SND_MIXER_SCHN_MONO, &r);
		snd_mixer_selem_get_capture_switch(d->elem, SND_MIXER_SCHN_FRONT_LEFT, &d->enabled);
	} else {
		snd_mixer_selem_get_playback_volume(d->elem, SND_MIXER_SCHN_FRONT_LEFT, &l);
		snd_mixer_selem_get_playback_volume(d->elem, SND_MIXER_SCHN_FRONT_RIGHT, &r);
		snd_mixer_selem_get_playback_switch(d->elem, SND_MIXER_SCHN_FRONT_LEFT, &d->enabled);
	}
	d->vol = (l+r)*50/d->max;

	/* display */
	buf_clear();
	if (d->enabled || d->persistent) {
		buf_append("\033F{%06X}\033I{%d} \033%c%2ld%%\033r",
		           d->enabled ? 0x00FF00 : 0xFF0000,
		           d->enabled ? d->icons[d->vol/34+1] : d->icons[0],
		           d->enabled ? 'm' : 'r', d->vol);
	}

	return 0;
}
