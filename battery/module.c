#include <karuibar/karuibar.h>
#include <errno.h>
#include <string.h>
#include <math.h>
#include <stdbool.h>

/* macros */
#define PATH_DEFAULT "/sys/class/power_supply"

/* structures */
struct data {
	char path_base[BUFSIZE];
	char path_charge[BUFSIZE];
	char path_charge_full[BUFSIZE];
	char path_current[BUFSIZE];
	char path_status[BUFSIZE];
	char id[BUFSIZE];
	int acpi_use_real;
	uint32_t colour_charging;
	int icons[11];
	bool disabled;
};

/* functions */
int battery_init(struct module *mod);
static int battery_poll(struct module *mod);
static void battery_term(struct module *mod);
static int readval(char const *path, unsigned int *val);

/* implementation */
int
battery_init(struct module *mod)
{
	unsigned int i, r;
	struct data *d;
	unsigned int long icon_bitfields[11][15];
	char path_base[BUFSIZE];
	char file_charge[BUFSIZE], file_charge_full[BUFSIZE],
	     file_current[BUFSIZE], file_status[BUFSIZE];

	/* module data */
	mod->data = d = smalloc(sizeof(struct data), "module data");

	/* user configuration */
	(void) xresources_boolean("acpi_use_real", &d->acpi_use_real, 0);
	(void) xresources_colour("colour_charging", &d->colour_charging, 0x4499CC);
	(void) xresources_string("id", d->id, "BAT0");
	(void) xresources_string("path_base", path_base, PATH_DEFAULT);
	(void) xresources_string("file_current", file_current, "power_now");
	(void) xresources_string("file_status", file_status, "status");
	(void) xresources_string("file_charge", file_charge, "energy_now");
	(void) xresources_string("file_charge_full", file_charge_full,
	                         d->acpi_use_real ?
	                         "energy_full_design" : "energy_full");
	(void) snprintf(d->path_base, BUFSIZE, "%s/%s", path_base, d->id);
	(void) snprintf(d->path_charge, BUFSIZE, "%s/%s", d->path_base, file_charge);
	(void) snprintf(d->path_charge_full, BUFSIZE, "%s/%s", d->path_base, file_charge_full);
	(void) snprintf(d->path_current, BUFSIZE, "%s/%s", d->path_base, file_current);
	(void) snprintf(d->path_status, BUFSIZE, "%s/%s", d->path_base, file_status);

	/* icons */
	for (i = 0; i < 11; ++i) {
		icon_bitfields[i][0] = 8L;
		icon_bitfields[i][1] = 13L;
		icon_bitfields[i][2] = 0x3C;
		icon_bitfields[i][3] = (i == 10) ? 0xFF : 0xE7;
		for (r = 4; r < 14; ++r)
			icon_bitfields[i][r] = (i >= 13-r) ? 0xFF : 0x81;
		icon_bitfields[i][14] = 0xFF;
		d->icons[i] = register_icon(icon_bitfields[i]);
	}

	/* module callbacks */
	mod->poll = battery_poll;
	mod->term = battery_term;

	/* initial poll */
	return battery_poll(mod);
}

static int
battery_poll(struct module *mod)
{
	FILE *f;
	uint32_t col;
	struct data *d = mod->data;
	int discharging, h=0, m=0;
	unsigned int charge, charge_full, current;
	double capacity, hours;

	buf_clear();

	/* is battery there? */
	f = fopen(d->path_base, "r");
	if (f == NULL)
		return 0;
	fclose(f);

	/* status */
	if ((f = fopen(d->path_status, "r")) == NULL) {
		ERROR("could not open %s for reading: %s",
		      d->path_status, strerror(errno));
		return -1;
	}
	discharging = (fgetc(f) == 'D');
	(void) fclose(f);

	/* current/full charge, consumed current */
	if (readval(d->path_charge     , &charge     ) < 0)
		return -1;
	if (readval(d->path_charge_full, &charge_full) < 0)
		return -1;
	if (readval(d->path_current    , &current    ) < 0)
		return -1;

	/* capacity */
	capacity = (double) charge / charge_full * 100;
	col = discharging ? colour((int unsigned) capacity)
	                  : d->colour_charging;

	/* time left */
	if (current != 0) {
		if (discharging)
			hours = (double) charge / current;
		else
			hours = (double) (charge_full - charge) / current;
		h = (int) hours;
		m = (int) (fmod(hours, 1) * 60);
	}

	/* display */
	buf_append("\033F{%06X}%.2f%% \033I{%d}\033r",
	           col, capacity, d->icons[(int) capacity/10]);
	if (current != 0)
		buf_append(" \033F{%06X}%d:%02d\033r (%2.2fW)",
		           karuibar.xresources.emphasised, h, m,
		           (float) current / 1000000);
	return 0;
}

static void
battery_term(struct module *mod)
{
	sfree(mod->data);
}

static int
readval(char const *path, unsigned int *val)
{
	FILE *f;
	int ret;

	if ((f = fopen(path, "r")) == NULL) {
		ERROR("could not open %s for reading: %s",
		      path, strerror(errno));
		return -1;
	}
	ret = fscanf(f, "%u", val);
	if (ret < 1) {
		ERROR("could read from %s: %s", path,
		      ret < 0 ? strerror(errno) : "failed match");
		(void) fclose(f);
		return -1;
	}
	(void) fclose(f);
	return 0;
}
