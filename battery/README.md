karuibar:battery
=================

This module shows information about the battery (charge, remaining time and
power consumption). It adapts the colour, time and consumption indicator
according to the charge level and whether it's currently being charged.

![battery.png](scrot/battery.png)

X resource                          | Description
:-----------------------------------|:------------------------------------------
`karuibar.battery.acpi_use_real`    | Whether to use the design capacity to calculate the battery charge level (default: `false`).
`karuibar.battery.colour_charging`  | Colour for when the battery is being charged (default: `#4499CC`).
`karuibar.battery.id`               | Battery identifier (default: `BAT0`).
`karuibar.battery.path_base`        | Path to the battery interface (default: `/sys/class/power_supply`).
`karuibar.battery.path_charge`      | Interface file name for the current charge level (default: `energy_now`).
`karuibar.battery.path_charge_full` | Interface file name for the full charge level (default: `energy_full` or `energy_full_design` if `acpi_use_real`).
`karuibar.battery.path_current`     | Interface file name for the currently consumed energy (default: `power_now`).
`karuibar.battery.path_status`      | Interface file name for the battery charge status (default: `power_now`).

Module       | `battery`
:------------|:-------------------------------------------------
Author       | Tinu Weber ([ayekat](https://gitlab.com/ayekat/))
Dependencies | None
Callbacks    | `init`, `poll`, `term`
License      | GPLv3
