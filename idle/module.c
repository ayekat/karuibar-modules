#include <karuibar/karuibar.h>
#include <X11/extensions/scrnsaver.h>
#include <string.h>

/* structures */
struct data {
	XScreenSaverInfo *info;
	Display *dpy;
	Window root;
	unsigned int idle; /* in milliseconds */
};

/* functions */
int idle_init(struct module *m);
static int idle_poll(struct module *mod);
static void idle_term(struct module *m);

int
idle_init(struct module *m)
{
	struct data *d;

	/* module data */
	m->data = d = smalloc(sizeof(struct data), "module data");

	/* X related data */
	d->info = XScreenSaverAllocInfo();
	if (d->info == NULL) {
		ERROR("could not allocate %zu bytes for X screensaver info",
		      sizeof(XScreenSaverInfo));
		return -1;
	}
	d->dpy = XOpenDisplay(NULL);
	if (d->dpy == NULL) {
		ERROR("could not open X display");
		XFree(d->info);
		return -1;
	}
	d->root = DefaultRootWindow(d->dpy);

	/* module callbacks */
	m->poll = idle_poll;
	m->term = idle_term;

	/* initial poll */
	return idle_poll(m);
}

static int
idle_poll(struct module *mod)
{
	struct data *d = mod->data;
	int long unsigned h, m, s;

	/* get info */
	XScreenSaverQueryInfo(d->dpy, d->root, d->info);
	h  = d->info->idle / 1000 / 60 / 60;
	m  = d->info->idle / 1000 / 60 % 60;
	s  = d->info->idle / 1000 % 60;

	/* update buffer */
	buf_clear();
	if (h > 0)
		buf_append("%lu:", h);
	buf_append("%0*lu:", strlen(mod->buf) > 0 ? 2 : 1, m);
	buf_append("%02lu", s);

	return 0;
}

static void
idle_term(struct module *m)
{
	struct data *d = m->data;

	XCloseDisplay(d->dpy);
	XFree(d->info);
	sfree(d);
}
