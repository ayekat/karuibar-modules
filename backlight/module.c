#include <karuibar/karuibar.h>
#include <sys/inotify.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

/* macros */
#define BACKLIGHT_BASE_PATH "/sys/class/backlight"

/* structures */
struct data {
	char path_brightness[BUFSIZE];
	int max_brightness;
	int icons[4];
};

/* functions */
int backlight_init(struct module *m);
static int backlight_react(struct module *m);
static void backlight_term(struct module *m);
static int read_numeric(char const *path);
static int update(struct data *d);

/* variables */
int long unsigned const icon_bitmaps[4][15] = {
	{ 11, 13, 0x000, 0x000, 0x000, 0x000, 0x070, 0x0F8, 0x0F8, 0x0F8, 0x070,
	          0x000, 0x000, 0x000, 0x000 },
	{ 11, 13, 0x000, 0x000, 0x020, 0x104, 0x070, 0x0F8, 0x2FA, 0x0F8, 0x070,
	          0x104, 0x020, 0x000, 0x000 },
	{ 11, 13, 0x000, 0x020, 0x222, 0x104, 0x070, 0x0F8, 0x6FB, 0x0F8, 0x070,
	          0x104, 0x222, 0x020, 0x000 },
	{ 11, 13, 0x000, 0x0A8, 0x222, 0x104, 0x471, 0x0F8, 0x6FB, 0x0F8, 0x471,
	          0x104, 0x222, 0x0A8, 0x000 },
};

int
backlight_init(struct module *m)
{
	struct data *d;
	char bldev[BUFSIZE], bfile[BUFSIZE], path_max_brightness[BUFSIZE];
	int unsigned i;

	/* module data */
	m->data = d = smalloc(sizeof(struct data), "module data");

	/* user configuration */
	(void) xresources_string("device", bldev, "acpi_video0");
	(void) xresources_string("file", bfile, "brightness");
	(void) snprintf(d->path_brightness, BUFSIZE,
	                BACKLIGHT_BASE_PATH"/%s/%s", bldev, bfile);
	(void) snprintf(path_max_brightness, BUFSIZE,
	                BACKLIGHT_BASE_PATH"/%s/max_brightness", bldev);

	/* module callbacks */
	m->react = backlight_react;
	m->term = backlight_term;

	/* set up inotify */
	m->fd = inotify_init();
	if (m->fd <= 0) {
		ERROR("could not initialise inotify");
		return -1;
	}
	if (inotify_add_watch(m->fd, d->path_brightness, IN_MODIFY) < 0) {
		ERROR("could not establish watch");
		return -1;
	}

	/* get max brightness */
	d->max_brightness = read_numeric(path_max_brightness);
	if (d->max_brightness < 0) {
		ERROR("could not read maximum brightness value");
		return -1;
	}

	/* icons */
	for (i = 0; i < LENGTH(icon_bitmaps); ++i)
		d->icons[i] = register_icon(icon_bitmaps[i]);

	/* initial poll */
	return update(d);
}

static int
backlight_react(struct module *m)
{
	char buf[BUFSIZE];

	if (read(m->fd, buf, BUFSIZE) <= 0) {
		ERROR("could not read from file descriptor");
		return -1;
	}

	return update(m->data);
}

static void
backlight_term(struct module *m)
{
	sfree(m->data);
	close(m->fd);
}

static int
read_numeric(char const *path)
{
	FILE *f;
	int n;

	f = fopen(path, "r");
	if (f == NULL) {
		ERROR("could not open %s: %s", path, strerror(errno));
		return -1;
	}
	if (fscanf(f, "%d", &n) < 1) {
		ERROR("could not read from %s: %s", path, strerror(errno));
		(void) fclose(f);
		return -1;
	}
	(void) fclose(f);
	return n;
}

static int
update(struct data *d)
{
	int brightness, percent;

	/* update data */
	brightness = read_numeric(d->path_brightness);
	if (brightness < 0) {
		ERROR("could not read current brightness value");
		return -1;
	}
	percent = 100 * brightness / d->max_brightness;

	/* update buffer */
	buf_clear();
	buf_append("\033I{%d} \033m%d%%",
	           d->icons[MIN(percent, 99) / 25], percent);

	return 0;
}
