karuibar:backlight
==================

This module displays the percentage of the screen's physical backlight
brightness in comparison with the maximum brightness.

![backlight.gif](scrot/backlight.gif)

X resource                  | Description
:---------------------------|:-----------------------------------------------------------------
`karuibar.backlight.device` | Battery device in `/sys/class/backlight` (default: `acpi_video0`)
`karuibar.backlight.file`   | File containing the brightness values `/sys/class/backlight/{device}` (default: `brightness`)

Module       | `backlight`
:------------|:-------------------------------------------------
Author       | Tinu Weber ([ayekat](https://gitlab.com/ayekat/))
Dependencies | none
Callbacks    | `init`, `react`, `term`
License      | GPLv3
